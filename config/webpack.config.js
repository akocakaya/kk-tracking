const path = require("path");

module.exports = {
    mode    : 'development',                                // production / none
    entry   : path.resolve(__dirname, '../src', 'app/index.jsx'),        // root file of our app
    output  : {
        path        : path.resolve(__dirname, '../dist'),      // folder name
        filename    : 'bundle.js',                          // compiled file name
        publicPath  : '/'                                   // where our code expects to find this file (root)
    },
    resolve : {
        extensions : ['.js', '.jsx']
    },
    devServer : {
        historyApiFallback : true                           // for react router to change the url in the browser by using javascript
    },
    module  : {
        rules   : [{
            test    : /\.jsx?/,                             // any file with .js or maybe x at the end, which means js or jsx, matches the test
            loader  : 'babel-loader',
            options : {
                "plugins": ["@babel/plugin-syntax-dynamic-import"]
            }
        }]
    }
}
