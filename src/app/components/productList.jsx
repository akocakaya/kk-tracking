import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import { requestProductCreation } from '../store/mutations';

class ProductList extends React.Component {

    render() {
        let products = this.props.products;

        return (
            <React.Fragment>

                {
                    products.map( p =>
                        <div key = { p.id } >
                            <Link to = {`/product/${p.id}`} key = { p.id } >
                                { p.name }
                            </Link>
                        </div>
                    )
                }

                <button onClick = { () => (this.props.createNewProduct()) } >
                    Create new product 
                </button>

            </React.Fragment>
        )
    }
}

function mapDispatchToProps(dispatch) {
    return {
        createNewProduct() {
            dispatch(requestProductCreation());
        }
    }
}

function mapStateToProps(state) {
    return {
        products : state.products
    }
}

export default connect(mapStateToProps, mapDispatchToProps) (ProductList);
