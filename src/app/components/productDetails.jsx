import React from 'react';
import { connect } from 'react-redux';

class ProductDetails extends React.Component {

    render() {
        let product = this.props.product;

        return (
            <React.Fragment>
                <div>
                    {product.id}
                </div>
                <div>
                    {product.name}
                </div>
                <div>
                    {product.price} $
                </div>
            </React.Fragment>
        )
    }
}

function mapStateToProps(state, ownProps) {
    let productId = ownProps.match.params.id;

    let product = state.products.find( p => p.id === productId);

    return {
        product
    }
}

export default connect(mapStateToProps) (ProductDetails);