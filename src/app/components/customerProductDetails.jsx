import React from 'react';
import { connect } from 'react-redux';

import { requestRemoveProductFromCustomer } from '../store/mutations';

class CustomerProductDetails extends React.Component {

    render() {
        let p = this.props;

        let product = p.product;

        return (
            <React.Fragment>
                <div>
                    <button onClick={ () => p.removeProductFromCustomer() }>
                        -
                    </button>
                    {product.id}
                </div>
                <div>
                    {product.name}
                </div>
                <div>
                    {product.price}
                </div>
                <br/>
            </React.Fragment>
        );
    }
}

function mapDispatchToProps(dispatch, ownProps) {
    return {
        removeProductFromCustomer() {
            dispatch(requestRemoveProductFromCustomer(ownProps.customerId, ownProps.product.id))
        }
    }
}

export default connect(null, mapDispatchToProps) (CustomerProductDetails);
