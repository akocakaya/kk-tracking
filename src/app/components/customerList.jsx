import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

class CustomerList extends React.Component {

    render() {

        let customers = this.props.customers;

        return (
            <React.Fragment>
                {
                    customers.map(
                        customer =>
                            <Link to={`/customer/${customer.id}`} key = { customer.id } >
                                <div>
                                    { customer.name }
                                </div>
                            </Link>
                    )
                }
            </React.Fragment>
        );
    }
}

function mapStateToProps(state, ownProps) {
    return {
        customers : state.customers.filter( customer => customer.userId === ownProps.userId)
    };
}

export default connect(mapStateToProps) (CustomerList);
