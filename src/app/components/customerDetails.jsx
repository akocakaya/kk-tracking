import React from 'react';
import { connect } from 'react-redux';

import CustomerProductList from './customerProductList';

class CustomerDetails extends React.Component {

    getTotalPrice() {
        let p = this.props;

        let customer = p.customers.find( customer => customer.id === p.id );
        let customerProductsDetail = customer.products.map(pr => p.products.find( sp => sp.id === pr));
        
        return customerProductsDetail.map(t => t.price).reduce((a, b) => a + b, 0);
    }

    render() {
        let p = this.props;

        let customer = p.customers.find( customer => customer.id === p.id );
        
        let customerId = customer.id;

        return (
            <React.Fragment>

                <h2>Customer</h2>

                <div>
                    {customer.id}
                </div>

                <div>
                    <input 
                        onChange = { (e) => console.log(e.target.value) }
                        value    = { customer.name } />
                    Products Price Sub =>   { this.getTotalPrice() } $
                </div>

                <CustomerProductList 
                    customerId       = { customerId } 
                    customerProducts = { customer.products } />

            </React.Fragment>
        )
    }
}

function mapStateToProps(state, ownProps) {
    return {
        customers   : state.customers,
        id          : ownProps.match.params.id,
        products    : state.products
    };
}

export default connect(mapStateToProps) (CustomerDetails);
