import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

class Navigation extends React.Component {

    render() {
        return (
            <React.Fragment>
                <Link to="/">
                    <h1>
                        Goes root
                    </h1>
                </Link>
                
                <Link to="/dashboard">
                    <h1>
                        Goes dashboard
                    </h1>
                </Link>

                <Link to="/products">
                    <h1>
                        Goes products
                    </h1>
                </Link>
            </React.Fragment>
        )
    }
}

export default connect(state => state) (Navigation);
