import React from 'react';
import { connect } from 'react-redux';

import { requestCustomerCreation } from '../store/mutations';
import CustomerList from './customerList';

class Dashboard extends React.Component {
    constructor() {
        super();
        this.state = { }
    }

    render() {
        let userId = 'u1';

        return (

            <React.Fragment> 

                <CustomerList userId={ userId } />


                <button onClick = { () => this.props.createNewCustomer(userId) } >
                    Create new customer 
                </button>

            </React.Fragment>

        )
    }
}

function mapDispatchToProps(dispatch) {
    return {
        createNewCustomer(userId) {
            dispatch(requestCustomerCreation(userId));
        }
    };
}

export default connect(null, mapDispatchToProps) (Dashboard);
