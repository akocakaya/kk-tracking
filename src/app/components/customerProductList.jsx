import React from 'react';
import { connect } from 'react-redux';

import { requestProductAddingToCustomer } from '../store/mutations';
import CustomerProductDetails from './customerProductDetails';

class CustomerProductList extends React.Component {

    getProductsButtons() {
        let p = this.props;

        let customerId = p.customerId;
        let products = p.products;

        return (
            products.map( product => 
                <button onClick = { () => { p.addProductToCustomer(customerId, product) } } key = { product.id } >
                    Add {product.name}
                </button>
            )
        )
    }

    getCustomerProductsDetail() {
        let p = this.props;

        let customerId = p.customerId;
        let cProducts = p.thisCustomerProducts;

        return (
            cProducts.map( p => 
                <CustomerProductDetails 
                    key = { Math.floor(Math.random() * 999) } 
                    product = { p } 
                    customerId = { customerId } /> 
            )
        )
    }

    render() {        
        return (
            <React.Fragment>

                <h3>Products</h3>

                { this.getProductsButtons() }

                { this.getCustomerProductsDetail() }

            </React.Fragment>
        );
    }
}

function mapDispatchToProps(dispatch) {
    return {
        addProductToCustomer(customerId, product) {
            dispatch(requestProductAddingToCustomer(customerId, product));
        }
    }
}

function mapStateToProps(state, ownProps) {

    let thisCustomerProducts = ownProps.customerProducts.map(
        customerProduct => state.products
            .find( sp => 
                sp.id === customerProduct )
    );

    return {
        products : state.products,
        thisCustomerProducts
    };
}

export default connect(mapStateToProps, mapDispatchToProps) (CustomerProductList);
