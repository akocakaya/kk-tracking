import React, { Suspense } from 'react';
import { Provider } from 'react-redux';
import { store } from '../store';
import { Router, Route } from 'react-router-dom';
import { history } from '../store/history';

import Navigation from './navigation';
const Dashboard = React.lazy( () => import('./dashboard'));
const ProductList = React.lazy( () => import('./productList'));
const CustomerDetails = React.lazy( () => import('./customerDetails'));
const ProductDetails = React.lazy( () => import('./productDetails'));

class Main extends React.Component {

    render() {
        return (
            <Router history = { history } >
                <Suspense fallback={<div>Loading...</div>}>
                    <Provider store = { store } >
                        <React.Fragment>

                            <Navigation />

                            <Route 
                                exact
                                path        = "/dashboard"
                                component   = { Dashboard } />

                            <Route 
                                exact
                                path        = "/customer/:id" 
                                component   = { CustomerDetails } />

                            <Route 
                                exact
                                path        = "/products" 
                                component   = { ProductList } />

                            <Route 
                                exact
                                path        = "/product/:id" 
                                component   = { ProductDetails } />
                                
                        </React.Fragment>
                    </Provider>
                </Suspense>
            </Router>
        )
    }
}

export default (Main);
