import { createStore, applyMiddleware, combineReducers } from 'redux';
import { createLogger } from 'redux-logger';
import createSagaMiddleware from 'redux-saga';

const sagaMiddleware = createSagaMiddleware();
import * as sagas from './sagas';

import { reducers } from './reducers';

export const store = createStore(
    combineReducers(reducers),
    applyMiddleware(/*createLogger(), */sagaMiddleware)
);

for(let saga in sagas) {
    sagaMiddleware.run(sagas[saga])
}
