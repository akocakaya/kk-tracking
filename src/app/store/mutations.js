export const REQUEST_CUSTOMER_CREATION = 'REQUEST_CUSTOMER_CREATION';
export const CREATE_CUSTOMER = 'CREATE_CUSTOMER';

export const REQUEST_ADD_PRODUCT_TO_CUSTOMER = 'REQUEST_ADD_PRODUCT_TO_CUSTOMER';
export const ADD_PRODUCT_TO_CUSTOMER = 'ADD_PRODUCT_TO_CUSTOMER';

export const REQUEST_REMOVE_PRODUCT_FROM_CUSTOMER = 'REQUEST_REMOVE_PRODUCT_FROM_CUSTOMER';
export const REMOVE_PRODUCT_FROM_CUSTOMER = 'REMOVE_PRODUCT_FROM_CUSTOMER';

export const REQUEST_PRODUCT_CREATION = 'REQUEST_PRODUCT_CREATION';
export const CREATE_PRODUCT = 'CREATE_PRODUCT';

export const requestCustomerCreation = (customerFromDashboard) => ({
    type : REQUEST_CUSTOMER_CREATION,
    userId : customerFromDashboard
});

export const createCustomer = (customerId, customerName, userId) => ({
    type : CREATE_CUSTOMER,
    customerId,
    customerName,
    userId
});

export const requestProductAddingToCustomer = (customerId, product) => ({
    type : REQUEST_ADD_PRODUCT_TO_CUSTOMER,
    customerId : customerId,
    product : product
});

export const addProductToCustomer = (customerId, product) => ({
    type : ADD_PRODUCT_TO_CUSTOMER,
    customerId : customerId,
    product : product
});

export const requestRemoveProductFromCustomer = (customerId, productId) => ({
    type : REQUEST_REMOVE_PRODUCT_FROM_CUSTOMER,
    customerId : customerId,
    productId : productId
});

export const removeProductFromCustomer = (customerId, productId) => ({
    type : REMOVE_PRODUCT_FROM_CUSTOMER,
    customerId : customerId,
    productId : productId
});

export const requestProductCreation = () => ({
    type : REQUEST_PRODUCT_CREATION
});

export const createProduct = (productId, productName, productPrice) => ({
    type : CREATE_PRODUCT,
    productId : productId,
    productName : productName,
    productPrice : productPrice
});
