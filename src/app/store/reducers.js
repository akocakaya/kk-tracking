import { defaultState } from '../../server/db/defaultState';
import * as mutations from './mutations';


export const reducers = {

    customers(customers = defaultState.customers, action) {
        switch(action.type) {

            case mutations.CREATE_CUSTOMER:
                return [...customers, {
                    id : action.customerId,
                    name : action.customerName,
                    userId : action.userId,
                    products : []
                }];

            case mutations.ADD_PRODUCT_TO_CUSTOMER:
                return customers.map( customer => {
                    return customer.id === action.customerId
                        ? { ...customer, 
                            products : [ ...customer.products, action.product.id] 
                        }
                        : customer;
                });

            case mutations.REMOVE_PRODUCT_FROM_CUSTOMER:
                return customers.map( customer => {
                    if(customer.id === action.customerId)
                        customer.products.splice(customer.products.indexOf(action.productId), 1);
                    return customer.id === action.customerId
                        ? { ...customer, 
                            products : customer.products
                        }
                        : customer;
                });

        }
        return customers;
    },

    products(products = defaultState.products, action) {
        switch(action.type) {

            case mutations.CREATE_PRODUCT:
                return [...products, {
                    id : action.productId,
                    name : action.productName,
                    price : action.productPrice
                }];

        }
        return products;
    }

}
