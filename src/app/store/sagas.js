import {
    take
}            from 'redux-saga/effects';
import uuid  from 'uuid';
import axios from 'axios';

import * as mutations from './mutations';

const url = "http://localhost:3412";

export function* customerCreationSaga() {
    while (true) {
        const { userId } = yield take(mutations.REQUEST_CUSTOMER_CREATION);


        yield axios.post(url + '/customer/create', {
            customer : {
                id : uuid(),
                name : 'customerName',
                userId : userId,
                products : []
            }
        });
    }
}