import {
    take,
    put
} from 'redux-saga/effects';
import uuid from 'uuid';

import * as mutations from './mutations';

export function* customerCreationSaga() {
    while (true) {
        const { userId } = yield take(mutations.REQUEST_CUSTOMER_CREATION);


        yield put(mutations.createCustomer(
                                uuid(), 
                                'customerDefault', 
                                userId
        ));
    }
}

export function* productAddingToCustomerSaga() {
    while (true) {
        const { customerId, product } = yield take(mutations.REQUEST_ADD_PRODUCT_TO_CUSTOMER);


        yield put(mutations.addProductToCustomer(
                                customerId,
                                product
        ));
    }
}

export function* productRemovingFromCustomerSaga() {
    while (true) {
        const { customerId, productId } = yield take(mutations.REQUEST_REMOVE_PRODUCT_FROM_CUSTOMER);


        yield put(mutations.removeProductFromCustomer(
                                customerId,
                                productId
        ));
    }
}

export function* productCreationSaga() {
    while (true) {
        yield take(mutations.REQUEST_PRODUCT_CREATION);

        yield put(mutations.createProduct(
                                uuid(),
                                'productDefault',
                                10
        ));
    }
}
