import express from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';

import { connectDB } from './db/connect-db';

let app = express();
let port = 3412;


app.listen(port, console.log("Server listening on port ", port));


app.use(
    cors(),
    bodyParser.urlencoded({extended:true}),
    bodyParser.json()
);

app.post('/customer/create', async (req, res) => {

    let db = await connectDB();

    //customer table
    let collection = db.collection('customers');
    //add new customer
    await collection.insertOne(req.body.customer);

    res.status(200).send();
});
