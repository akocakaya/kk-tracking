export const defaultState = {
	"users": [
        {
            "id"   : "u1",
            "name" : "user1"
        },
        {
            "id"   : "u2",
            "name" : "user2"
        }
    ],
	"customers": [
        {
            "id"   : "c1",
            "name" : "customer1",
            "userId" : "u1",
            "products" : [
                "p1",
                "p1",
                "p4"
            ]
        },
        {
            "id"   : "c2",
            "name" : "customer2",
            "userId" : "u1",
            "products" : [
                "p2",
                "p3",
                "p4"
            ]
        },
        {
            "id"   : "c3",
            "name" : "customer3",
            "userId" : "u1",
            "products" : [
                "p2",
                "p2",
                "p2"
            ]
        },
        {
            "id"   : "c4",
            "name" : "customer4",
            "userId" : "u2",
            "products" : [
                "p2",
                "p2",
                "p4"
            ]
        },
        {
            "id"   : "c5",
            "name" : "customer5",
            "userId" : "u2",
            "products" : [
                "p3",
                "p1",
                "p1"
            ]
        },
        {
            "id"   : "c6",
            "name" : "customer6",
            "userId" : "u1",
            "products" : [ ]
        },
        {
            "id"   : "c7",
            "name" : "customer7",
            "userId" : "u2",
            "products" : [ ]
        }
    ],
    "products" : [
        {
            "id"   : "p1",
            "name" : "product1",
            "price": 10.05
        },
        {
            "id"   : "p2",
            "name" : "product2",
            "price": 20.04
        },
        {
            "id"   : "p3",
            "name" : "product3",
            "price": 30.03
        },
        {
            "id"   : "p4",
            "name" : "product4",
            "price": 40.01
        }
    ]
}